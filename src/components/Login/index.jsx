import { LoginBox } from "./style";
import { useState } from "react";
import axios from "axios";
import toast, { Toaster } from "react-hot-toast";

const Login = () => {
  const [userLogin, setUserLogin] = useState("");
  const [userPassword, setUserPassword] = useState("");

  const formData = {
    username: userLogin,
    password: userPassword,
  };

  const onSubmit = (event) => {
    event.preventDefault();
    if (userLogin.trim() !== "" && userPassword.trim() !== "") {
      axios
        .post("https://kenzieshop.herokuapp.com/sessions/", formData)
        .then((response) => {
          if (response.status === 200) {
            toast.success(`Bem vindx, ${userLogin}!`);
          } else {
            toast.error("Ops, algo de errado aconteceu");
          }
        })
        .catch(() => toast.error("Dados invalidos"));
    } else {
      toast.error("Insira seus dados");
    }
  };

  return (
    <LoginBox>
      <Toaster />
      <h3>Login</h3>
      <form onSubmit={(event) => onSubmit(event)}>
        <label>Login</label>
        <input
          type="text"
          placeholder="Login"
          value={userLogin}
          onChange={(event) => setUserLogin(event.target.value)}
        />

        <label>Senha</label>
        <input
          type="password"
          placeholder="Senha"
          value={userPassword}
          onChange={(event) => setUserPassword(event.target.value)}
        />

        <button>Enviar!</button>
      </form>
    </LoginBox>
  );
};

export default Login;
