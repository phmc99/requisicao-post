import styled from "styled-components";

export const LoginBox = styled.div`
  box-sizing: border-box;
  width: 300px;
  padding: 30px;
  background-color: lightblue;
  text-align: center;
  font-family: sans-serif;
  border: 10px double lightcyan;
  box-shadow: 0 0 5px gray;
  color: rgb(36, 36, 36);

  position: absolute;
  top: 50%;
  left: 50%;
  transform: translate(-50%, -50%);

  border-radius: 15px;

  h3 {
    margin: 5px 0;
  }

  form {
    text-align: left;
    display: flex;
    flex-direction: column;

    label {
      font-size: 12px;
    }

    input {
      margin-top: 5px;
      border-radius: 5px;
      border: 0;
      margin-bottom: 10px;
      padding: 5px;

      &:focus {
        transition: all 0.2s;
        transform: scale(105%);
      }
    }

    button {
      border-radius: 5px;
      background-color: lightyellow;
      border: 0;
      align-self: center;
      width: 80px;
      margin-top: 10px;
      padding: 5px;
      cursor: pointer;
      font-size: 14px;
      border: 2px inset lightgray;

      &:hover {
        background-color: gold;
        transition: all 0.2s;
        border: 2px outset lightgray;
      }
      &:active {
        border: 2px inset lightgray;
      }
    }
  }
`;
